<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>陣列反轉</title>
</head>
<body>
    <?php
        $array = [2, 4, 6, 1, 8];
        $count = count($array);
        
        echo "轉換前：" . implode($array);

        for ($index = 0; $index < $count / 2; $index++) {
            $temp = $array[$index];
            $array[$index] = $array[($count -1) - $index];
            $array[($count -1) - $index] = $temp;
        }

        echo "<br/>轉換後：" . implode($array);
    ?>
</body>
</html>