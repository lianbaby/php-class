<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>威力彩</title>
</head>
<body>
    <?php
        // 威力彩第一組以及第二組分別有幾組數字
        $group1_count = 6;
        $group2_count = 1;
        // 威力彩第一組以及第二組的最小數
        $group1_min = 1;
        $group2_min = 1;
        // 威力彩第一組以及第二組的最大數
        $group1_max = 42;
        $group2_max = 6;

        $group1 = array();
        $group2 = array();

        while (count($group1) < $group1_count) {
            // 獲取亂數
            $rand = rand($group1_min, $group1_max);
            // 檢查是否存在於陣列之中，如果不存在則新增
            if (!in_array($rand, $group1)) {
                array_push($group1, $rand);
            }
        }

        while (count($group2) < $group2_count) {
            // 獲取亂數
            $rand = rand($group2_min, $group2_max);
            // 檢查是否存在於陣列之中，如果不存在則新增
            if (!in_array($rand, $group2)) {
                array_push($group2, $rand);
            }
        }

        sort($group1);
        sort($group2);

        echo "您的號碼為：" . join(", ", $group1);

        if ($group2_count > 0) {
            echo ", 特別號為：" . join(", ", $group2);
        }
    ?>
</body>
</html>