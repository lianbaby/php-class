# PHP 相關作業
#### 流程控制
 - [菱形](./class2/diamond.php)

#### 陣列
 - [學生成績](./class3/student.php)
 - [九九乘法表](./class3/multiplication-table.php)
 - [威力彩](./class3/lottery.php)
 - [近五百年的閏年](./class3/leap-yaer.php)
 - [天干地支](./class3/sexagenary.php)
 - [陣列反轉](./class3/reverse-array.php)

 #### 字串
 - 字串取代
 - 字串分割
 - 字串組合
 - 子字串取用
 - 尋找字串與HTML、css整合應用

 #### 日期
  - [日曆](./calendar.php)
  - [萬年曆](./calendar2.php)
