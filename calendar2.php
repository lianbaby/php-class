<html lang="en">
<?php
    $json_str = file_get_contents('./schedule.json');
    $schedules = json_decode($json_str);

    $max_weeks_of_a_month = 6;
    $days_of_a_week = 7;
    $current_date = date('Y-m');
    $date = isset($_GET["date"]) ? $_GET["date"] : $current_date;
    
    $first_day = date('w', strtotime($date . "-1"));
    $year = date('Y', strtotime($date . "-1"));
    $month = date('m', strtotime($date . "-1"));
    $today = date('Ymd');
    $total_day_of_this_month = date('t', strtotime($date . "-1"));

    $next_date = date('Y-m', strtotime("+1 month", strtotime($date . "-1")));
    $prev_date = date('Y-m', strtotime("-1 month", strtotime($date . "-1")));
    
?>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>萬年曆</title>
    <style>
        .calendar .holiday {
            background-color: fb6161;
        }

        .calendar .today {
            
        }

        .calendar .desc {
            
        }

        .calendar .week {

        }

        .calendar .date {
            
        }
    </style>
</head>
<body>
    
    <div>
        <a href="?date=<?php echo $prev_date ?>">Prev</a>    
        <a href="?date=<?php echo $next_date ?>">Next</a>    
        <a href="?date=<?php echo $current_date ?>">Today</a>    
    </div>
    <div>
        <?php echo $year ?> 年
        <?php echo $month ?> 月
    </div>
    <div>
        <table class='calendar'>
            <thead>
                <tr>
                    <th>日</th>
                    <th>ㄧ</th>
                    <th>二</th>
                    <th>三</th>
                    <th>四</th>
                    <th>五</th>
                    <th>六</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    for ($week = 0; $week < $max_weeks_of_a_month; $week++) {
                        $start_date_of_this_week = ($week * $days_of_a_week) - ($first_day);
                        echo "<tr class='week'>";

                        for ($day = 1; $day <= $days_of_a_week; $day ++) {
                            $date_of_this_month = $start_date_of_this_week + $day;
                            
                            if ($date_of_this_month < 1 || $date_of_this_month > $total_day_of_this_month) {
                                echo "<td class='date'></td>";
                                continue;
                            }

                            $desc = '';
                            $is_holiday = false;
                            $full_date = date('Ymd', strtotime($date . '-' . $date_of_this_month));

                            if (isset($schedules -> { $full_date })) {
                                $schedule = $schedules -> { $full_date };

                                if (isset($schedule -> { 'is_holiday' })) {
                                    $is_holiday = $schedule -> { 'is_holiday' } == 2;
                                }

                                if (isset($schedule -> { 'desc' })) {
                                    $desc = $schedule -> { 'desc' };
                                }
                            }

                            $classes = [];
                            if ($is_holiday) {
                                array_push($classes, 'holiday');
                            }
                            if ($today === $full_date) {
                                array_push($classes, 'today');
                            }

                            echo "<td class='date " . join(' ', $classes) ."'>";
                            echo "  $date_of_this_month";
                            echo "  <div class='desc'>$desc</div>";
                            echo "</td>";
                        }

                        echo "</tr>";
                    }
                ?>
            </tbody>
        </table>
    </div>
</body>
</html>
