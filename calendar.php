<?php 
    // 當前年份
    $current_year = date('Y');
    // 當前月份
    $current_month = date('n');
    // 當前日期
    $current_date = date('j');
    // 把日期字串轉換成日期，獲取當前年月的第一天。
    $first_date = strtotime($current_year . "-" . $current_month . "-1");
    // 一個月一共有幾天
    $days_of_a_month = date('t',  $first_date);
    // 獲取日期是在這周是第幾天，星期一為第一天，星期天為第七天。
    $start_day_of_a_week = date('N',  $first_date);
    // 一個月有幾周
    $weeks_of_a_month = 6;
    // 一周有幾天
    $days_of_a_week = 7;
?>
    <table>
        <tr>
            <th>一</th>
            <th>二</th>
            <th>三</th>
            <th>四</th>
            <th>五</th>
            <th>六</th>
            <th>日</th>
        </tr
<?php
    for ($week = 0; $week < $weeks_of_a_month; $week++) {
        // 計算當前這周的起始日期。如果為負數則代表起始日期為上個月開始。
        // 由於獲取週數的第1天，起始為1，所以需要減掉1，從0開始
        $start_date_of_this_week = ($week * $days_of_a_week) - ($start_day_of_a_week - 1);

        echo "<tr>";

        for ($day = 1; $day <= $days_of_a_week; $day++) {
            // 計算當前日期
            $date = $start_date_of_this_week + $day;
            // 如果日期扣掉起始天數是小於0，或者日期大於計算月份的最大天數，則輸出空白。
            if ($date <= 0 || $date > $days_of_a_month) {
                echo "<td></td>";
            } else {
                echo "<td>" . $date . "</td>";
            }
        }

        echo "</tr>";
    }
?>

    </table>
